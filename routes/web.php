<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
}); */


Route::get('/', 'HomeController@home');

Route::get('/register','AuthController@form');

Route::post('/welcome','AuthController@welcome');

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/calendar', function(){
    return view('layout.calendar');
});

Route::get('/table', function(){
 return view('layout.table');
});

Route::get('/data-tables', function(){
    return view('layout.data_table');
   });