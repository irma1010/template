<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function welcome(Request $request){
        $fname = $request->fname;
        $lname = $request->lname;
        $gender = $request->gender;
        $nat = $request->nat;
        $lang = $request->lang;
        $bio = $request->bio;
        return view('welcom',compact('fname','lname','gender','nat','lang','bio'));
    }
}
